'use strict'
const Comment  = use('App/Model/Comment')
const Database = use('Database')
class CommentController {
  * getComments(request,response){
    const comments   = yield Database.table('users').innerJoin('comments', 'users.id', 'comments.user_id').select('users.email','comments.*')
    //const comments = yield Comment.innerJoin('users','users.id','commetns.user_id');
    return response.json(comments);
  }
  * saveComment(request,response){
    const comment_r = request.input("comment");
    var hoy =  new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1; //hoy es 0!
    var yyyy = hoy.getFullYear();
    hoy = yyyy+'/'+mm+'/'+dd;
    var user = yield request.auth.getUser();
    const comment = new Comment();
    comment.text = comment_r;
    comment.fecha = hoy;
    comment.user_id = user.id;
    yield comment.save();
    comment.email = user.email;
    return response.json(comment);
  }
}

module.exports = CommentController
