'use strict'


const Grupo = use('App/Model/grupo')
const Conv = use('App/Model/conversacion')

class gruposController {
    * save(request,response){
        const grupo = new Grupo()
        grupo.nombre = request.input('nombre')
        grupo.user_id = request.auth.getUser().id

        yield grupo.save()

        if (request.input('miembros').length > 0){
            for (var i=0; i < request.input('miembros').length; i++) {
                const conv = new Conv()
                conv.grupo_id = grupo.id
                conv.user_id = request.input('miembros')[i]
                yield conv.save()
            }
        }
        response.json({status:200,message:'Grupo registrado'})
        return
    }
    * save(request,response){
        const grupo = Grupo.findById(request.input('grupo_id'))
        grupo.nombre = request.input('nombre')
        grupo.user_id = request.auth.getUser().id

        yield grupo.save()

        if (request.input('miembros').length > 0){
            var miembrosAdd=request.input('miembros')
            var miembrosDel=request.input('miembrosDel')
            for (var i=0; i < miembrosAdd.length; i++) {
                const hasConv = Conv.query().where('user_id','=',miembrosAdd[i])
                if(!hasConv){
                    const conv = new Conv()
                    conv.grupo_id = grupo.id
                    conv.user_id = miembrosAdd[i]
                    yield conv.save()
                }
            }
            for (var i=0; i > miembrosDel.length; i++){
                const hasConv = Conv.query().where('user_id','=',miembrosDel[i])
                if(hasConv){
                    hasConv.delete()
                }
            }
            response.json({status:200,message:'Grupo registrado'})
        }
        return
    }
}

module.exports = gruposController
