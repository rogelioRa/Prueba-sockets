'use strict'

const User = use('App/Model/User')
const Hash = use('Hash')
const Database = use('Database')

class UserController {
  * login(request,response){
    const email = request.input('email')
    const password = request.input('password')
    const login = yield request.auth.attempt(email, password)
    if (login) {
      response.send('Logged In Successfully')
      return
    }
    response.unauthorized('Invalid credentails')
  }
  * obtener(req,res){
      console.log(req)
      res.send("aquí")
      return;
  }
  * save(request,response){
        const user = new User()

        user.username = request.input('username')
        user.email = request.input('email')
        user.password = request.input('password')

        yield user.save()

        var registerMessage = { success: user }

        response.json({message:"Usuario registrado con exito.",status:200,data:registerMessage.success})

        return
  }
  * viewRegister(request,response){
        yield response.sendView('register')
        return
  }
  * viewChat(request,response){
     let user = request.currentUser;
     console.log(user)
     if (user) {
        yield response.sendView('grupos',{ User : user})
         return
     }
     else{
         response.redirect('back')

     }

  }

  * findByUser (request,response){
      let params = request.params()
      var users = yield Database.from('users').where({username:params.user}).whereNot({'username':request.currentUser.username})
      response.json(users)
  }
}

module.exports = UserController
