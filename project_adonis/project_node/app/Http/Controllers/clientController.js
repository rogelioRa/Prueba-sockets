'use strict'


const Client = use('App/Model/client')

class clientController {
  * upload(request,response){
    const data = request.only('nombre', 'apellidos', 'direccion')
    /*const data = {
      nombre: request.input(2),
      apellidos:{
        paterno:"jinestas",
        materno:"garcía"
      },
      direccion:{
        calle:'juarez',
        numero:'123',
        colonia:'Jardines'
      }
    };*/
    Client.create(data)
    yield response.send('Registred In Successfully')
  }
  * obtener(req,res){
      const clients = yield Client.find();
      return res.json( clients );
  }
  * show(req,res){
    return yield res.sendView('uploadClient');
  }
  * showWs(request,response){
    return yield response.sendView('ws');
  }
}

module.exports = clientController
