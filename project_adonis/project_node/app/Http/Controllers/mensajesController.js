'use strict'

const File = use('File')
const   fs =  require('fs')
const Client = use('App/Model/client')
const Message = use('App/Model/Message')
const Helpers = use('Helpers')

class mensajesController {

    * getMessage(request,response){
        try {
          let messages = Message.findBy('room',response.input('room'))
          var data = null
          for (var i = 0; i < messages.length; i++){
              data += '   '+messages[i].user + 'envío el mensaje: '+messages[i].message+' al grupo: '+messages.room+'\n'
          }

          yield File.put('mensajes.txt',data)

          /*response.download(Helpers.publicPath('uploads/mensajes.txt'))
          return*/
        } catch (e) {

        }
    }

}

module.exports = mensajesController
