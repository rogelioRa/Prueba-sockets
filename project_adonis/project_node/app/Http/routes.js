'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/

const Route = use('Route')
const User = use('App/Model/User')
const Hash = use('Hash')
const Ws   = use('Ws')
Route.get("/precense",function *(req,res){
  yield res.send(JSON.stringify(Ws.channel("post").presence))
  return
})
Route.post('/login','UserController.login')
Route.get('logout',function *(req,res){
  const loggedUser = req.currentUser;
  if(!loggedUser){
    res.send("Logged out");
    return
  }
  const userSockets = Ws.channel("post").presence.pull(loggedUser.id,() => true);
  userSockets.forEach((socket)=>{
    socket.socket.disconnect();
  });
  yield req.auth.logout()
  res.redirect('/')
})
Route.get('/obtener','UserController.obtener')
Route.get('/getClients','clientController.obtener')

Route.post('/registrar','UserController.save')
Route.get('/registro-de-usuario','UserController.viewRegister')
Route.get('/chat','UserController.viewChat')
Route.get('find-by-user/:user','UserController.findByUser')

Route.group('auth-routes', () => {
  Route.get('prueba-ws', 'clientController.showWs')
//  Route.get('/registrar','clientController.upload')
  Route.get('/welcome','clientController.show')
  Route.get('/getComments','CommentController.getComments')
  Route.get('/sendComment','CommentController.saveComment')
  Route.post('/saveGroup','gruposController.save')
  Route.post('/download-messages','mensajesController.getMessage')
}).middleware('auth')

Route.on('/').render('welcome')


Route.get('/reg',function * (request,response){
  const user = new User()
  user.email ='gerson_17@live.com'
  user.password = '123456';
  yield user.save()
  response.json(user)
})
