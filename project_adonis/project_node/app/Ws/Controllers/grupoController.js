'use strict'

const Grupo = use('App/Model/Grupo')
const Conv = use('App/Model/Conversacion')
const Database = use("Database")
class grupoController {

  constructor (socket,request,presence) {
    this.socket    =   socket
    this.request   =   request
    this.presence  =   presence
    console.log("socket connected id %s",socket.id)
    presence.track(socket,socket.currentUser.id,{
      socket_id: socket.id
    })
  }
  * leaveRoom (room) {
    // Do cleanup if required
  }
  * onLeave(data){
    try {
      const grupo_r      = JSON.parse(data);
      const grupoFind    =  yield Grupo.findBy("id",grupo_r.id)
      const conversacionFind =  yield Database.from("conversaciones").where("user_id",this.socket.currentUser.id).where("grupo_id",grupo_r.id).first()
      if(grupoFind.user_id != this.socket.currentUser.id){
        console.log(conversacionFind)
        const conversacion = yield Conv.findBy("id",conversacionFind.id);
        const message = "El Usuario "+this.socket.currentUser.username;" ha abandonado el grupo";
        this.socket.toEveryone().inRoom(grupo_r.nombre).emit("leaveall",{status:200,statusMessage:"Usuario abandono grupo",message:message,data:grupo_r})
        this.socket.toMe().emit("leave",{status:200,statusMessage:"Usuario abandono grupo",message:message,data:grupo_r})
        yield conversacion.delete()
      }else{
        this.socket.toMe().emit("leave",{status:500,statusMessage:"No se pudo reealizar el abandono",message:"No puedes abandonar el grupo recuerda que eres el administrador"})
      }
    } catch (e) {
      console.log(e)
    }
  }
  * onDelete(data){
    try {
      const grupo_r   = JSON.parse(data);
      const grupoFind =  yield Grupo.findBy("id",grupo_r.id)
      if(grupoFind.user_id === this.socket.currentUser.id){
        yield grupoFind.delete();
        const message = "El grupo "+grupo_r.nombre+" ha sido eliminado por el usuario "+this.socket.currentUser.username;
        this.socket.toEveryone().inRoom(grupo_r.nombre).emit("delete",{status:200,statusMessage:"EL grupo se ha eliminado",message:message,data:grupo_r})
      }else{
        this.socket.toMe().emit("delete",{status:500,statusMessage:"EL grupo no se ha eliminado",message:"No tienes permisos para realizar esta acción solamente el administrador del grupo"})
      }
    } catch (e) {
      console.log(e)
    }
  }
     * onGreet(grupos){
       try {
         const grupos = yield Database.table("grupos")
                    .innerJoin("conversaciones","grupos.id","conversaciones.grupo_id")
                    .where("conversaciones.user_id",this.socket.currentUser.id).select("grupos.nombre","grupos.id");
         console.log(grupos);
         this.socket.toMe().emit("greet",JSON.stringify(grupos))
       } catch (e) {
           console.log(e)
       }
     }
  * onMessage(message) {
       try {
        const msg = JSON.parse(message)
        const grupo = new Grupo()
        grupo.nombre = msg.nombre
        grupo.user_id = this.socket.currentUser.id

        yield grupo.save()
        msg.miembros.push(grupo.user_id)
        if (msg.miembros.length > 0){
            for (var i=0; i < msg.miembros.length; i++) {
                const conv = new Conv()
                conv.grupo_id = grupo.id
                conv.user_id = msg.miembros[i]
                yield conv.save()
            }
        }
        msg.sockets.push(this.socket.id)
        this.socket.to(msg.sockets).emit("message",JSON.stringify({id:msg.id,nombre:msg.nombre}))
    }catch(e){
        console.log(e)
    }
  }
  * joinRoom (room) {
    //const user = this.socket.currentUser
    // throw error to deny a socket from joining room
  }
}
module.exports = grupoController
