'use strict'
const Mensaje  = use('App/Model/mensaje')
const Message = use('App/Model/Message')
const Database = use('Database')
const User = use('App/Model/User')

class ChatController {

  constructor (socket,request,presence) {
    this.socket = socket
    this.request = request
    console.log("socket connected id %s",socket.id)
    presence.track(socket,socket.currentUser.id,{
      email: socket.currentUser.email
    })
  }

  * onGreet(messages){
       let data = JSON.parse(messages)
       try {
         const msgs = yield Database.table("mensajes")
                    .innerJoin("grupos","grupos.id","mensajes.grupo_id")
                    .innerJoin("users","users.id","mensajes.user_id")
                    .where("grupos.id",data.id).select("mensajes.id as idMensaje","mensajes.content","users.username","users.id")
                    .orderBy('idMensaje', 'asc')
        console.log(msgs)
         this.socket.toMe().emit("greet",msgs)
       } catch (e) {
           console.log(e)
       }
  }

  * onTyping(typing){
      let data = typing
      let user = this.socket.currentUser;
      this.socket.toEveryone().inRoom(data.room).emit('typing',{username:user.username,group_id:data.group_id,user_id:user.id})
  }

  * onMessage (message) {
    try{
        let data = JSON.parse(message)
        let message_r = data.content
        let user_id   = this.socket.currentUser.id
        let mensaje   = new Mensaje()
        mensaje.content = message_r;
        mensaje.user_id = user_id;
        mensaje.grupo_id = data.id_group;
        let user = yield User.find(user_id)
        yield mensaje.save()
        const objDoc = {
            room:data.room,
            user:user.username,
            message:mensaje.content,
        };
        var MessageMDB = new Message(objDoc)
        MessageMDB.save()

        this.socket.toEveryone().inRoom(data.room).emit('message',{content:message_r,username:user.username,group_id:mensaje.grupo_id,user_id:user.id})
    }catch (e) {
        console.log(e)
    }

  }
  * joinRoom (room) {
    //const user = this.socket.currentUser
    // throw error to deny a socket from joining room
  }
}
module.exports = ChatController
