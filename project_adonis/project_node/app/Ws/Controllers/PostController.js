'use strict'
const Comment  = use('App/Model/Comment')
const Database = use('Database')

class PostController {

  constructor (socket,request,presence) {
    this.socket = socket
    this.request = request
    console.log("socket connected id %s",socket.id)
    presence.track(socket,socket.currentUser.id,{
      email: socket.currentUser.email
    })
  }


  * onMessage (message) {
    // listening for message event
    //this.socket.toEveryone().emit("message",message)
    console.log("recived message",message)
    const comment_r = message;
    var hoy =  new Date();
    var dd = hoy.getDate();
    var mm = hoy.getMonth()+1; //hoy es 0!
    var yyyy = hoy.getFullYear();
    hoy = yyyy+'/'+mm+'/'+dd;
    var user = yield this.request.auth.getUser();
    const comment = new Comment();
    comment.text = comment_r;
    comment.fecha = hoy;
    comment.user_id = user.id;
    yield comment.save();
    comment.email = user.email;
    this.socket.toEveryone().emit("message",JSON.stringify(comment))
  }
}
module.exports = PostController
