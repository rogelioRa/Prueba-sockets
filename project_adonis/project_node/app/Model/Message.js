'use strict'

/**
*  @var Mongoose mongoose
*/
const mongoose = use('Mongoose')

let MensajeSchema = mongoose.Schema({
    room: String,
    user: String,
    message: String

});

module.exports = mongoose.model('Message', MensajeSchema)
