const Hash = use('Hash')
const User = exports = module.exports = {}

User.encryptPassword = function * (next) {
  this.password = yield Hash.make(this.password)
  yield next
}

User.validate = function * (next) {
  if (!this.username) {
    throw new Error('Usuario es requerido.')
  }else if(!this.email){
    throw new Error('Email es requerido.')
  }else if(!this.password){
    throw new Error('Contraseña requerida.')
  }else if(this.password.length < 6){
    throw new Error('La contraseña no puede ser menor a 6 caracteres.')
  }
  yield next
}
