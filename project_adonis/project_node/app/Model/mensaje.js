'use strict'

const Lucid = use('Lucid')

class mensaje extends Lucid {
  static get table () {
    return 'mensajes'
  }
}

module.exports = mensaje
