'use strict'

/**
*  @var Mongoose mongoose
*/
const mongoose = use('Mongoose')

let client = mongoose.Schema({
    //id: ObjectId,
    nombre: String,
    apellidos: {
        paterno: String,
        materno: String
    },
    direccion:{
        calle: String,
        numero: String,
        colonia: String
    }
});

module.exports = mongoose.model('client', client)
