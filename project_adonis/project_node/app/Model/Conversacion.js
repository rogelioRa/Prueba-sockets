'use strict'

const Lucid = use('Lucid')

class Conversacion extends Lucid {
  static get table () {
    return 'conversaciones'
  }
}

module.exports = Conversacion
