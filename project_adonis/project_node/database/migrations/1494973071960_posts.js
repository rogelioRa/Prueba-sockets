'use strict'

const Schema = use('Schema')

class =TableSchema extends Schema {

  up () {
    this.create('=', (table) => {
      table.increments()
      table.timestamps()
    })
  }

  down () {
    this.drop('=')
  }

}

module.exports = =TableSchema
