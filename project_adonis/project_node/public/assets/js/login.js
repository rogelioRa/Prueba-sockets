$(function(){
  $("#btn-login").click(function(event){
    var _csrf = $("input[name='_csrf']").val();
    var data = {
      email: $("input[name='email']").val(),
      password: $("input[name='pass']").val(),
      _csrf : _csrf
    };
    $.ajax({
      "headers": { 'csrftoken' : _csrf },
      "url":"/login",
      "type":"post",
      "data":data
    }).done(function(response){
      document.location ="/chat";
      console.log(response);
      data = response;
    }).fail(function(error){
      alert("Fallo al autentificar user");
      console.log(error);
    });

  });
});
