$(function(){
  var datos = [];
  var _csrf = $("input[name='_csrf']").val();
  $.ajax({
    "headers": { 'csrftoken' : _csrf },
    url:"/getClients",
    type:"GET"
  }).done(function(res){
    fillTable(res);
  }).fail(function(error){
    console.error(error);
  });
  fillTable = function(data){
    $.each(data,function(index,object){
      console.log(object);
      var tr = $("<tr>"+
      "<td>"+object.nombre+"</td>"+
      "<td>"+object.apellidos.paterno+"</td>"+
      "<td>"+object.apellidos.materno+"</td>"+
      "<td>"+object.direccion.calle+" - "+object.direccion.colonia+" - "+object.direccion.numero+"</td>"+
      "</tr>"
      );
      $("#tb-datos tbody").append(tr);
    });
  }
  $("#btn-save-client").click(function(){
    var datos = {
      nombre : $("#name").val(),
      apellidos: {
        paterno : $("#apellido-pat").val(),
        materno : $("#apellido-mat").val()
      },
      direccion: {
        calle : $("#calle").val(),
        colonia : $("#colonia").val(),
        numero: $("#number").val()
      }
    };
    console.log(datos);
    $.ajax({
      "headers": { 'csrftoken' : _csrf },
      url: "/registrar",
      type:"GET",
      data: datos
    }).done(function(response){
      console.log(response);
      window.location.reload();
    }).fail(function(error){
      console.error(error)
    });
  });
});
