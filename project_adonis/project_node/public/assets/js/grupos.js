$(function(){
    var findUsers = function(){
       var $dato = $('body').find("#select-to").selectize({
            valueField: 'id',
            labelField: 'username',
            searchField: 'username',
            create: false,
            render: {
                option: function(item, escape) {
                    let theUsername = escape(item.username);
                    return "<div class='findedObj "+ theUsername +"'>" +
                        "<label class='iconPhoto' style='background:#3e5aa5; border-radius:100%;'>"+ theUsername.toLocaleUpperCase().substring(0, 1) +"</label><br>" +
                        "<label class='name'>" + theUsername + "</label>" +
                        '<label class="by"><span class="fa fa-caret-right"></span> &nbsp; ' + escape(item.email) + '</label>' +
                    '</div>';
                }
            },
            load: function(query, callback) {
                if (!query.length) return callback();
                $.ajax({
                    url: '/find-by-user/' + encodeURIComponent(query),
                    type: 'GET',
                    error: function(eror) {
                        callback();
                    },
                    success: function(res) {
                        callback(res.slice(0, 10));
                    }
                });
            }
        });

        return $dato[0].selectize;
    }
    findUsers();
    var finded = findUsers();
    client.emit("greet","Hello I want to get all groups for me");
    client.on("greet",function(grupos){
      console.log(grupos);
        let dts_grupos = JSON.parse(grupos)
        console.log(client_chat)
        $.each(dts_grupos,function(i,grupo){
            client.joinRoom(grupo.nombre, {}, function (error, joined) {
              $('#chatList').append(groupComponents.list.group(grupo));
            })
            client_chat.joinRoom(grupo.nombre,{},function(err,j){
            });
        })
    });

  const sockets = [];
  client.on('message',function(message){
    console.log(message);
    $('#new-group').modal('hide');
    var data = JSON.parse(message);
    client.joinRoom(data.nombre, {}, function (error, joined) {
        alertify.message("Se te agrego al grupo "+data.nombre)
        $('#chatList').append(groupComponents.list.group(data));
    });
  });

  client.on("presence:state",function(state){
    console.log(state);
    state.map(function(user){
      sockets.push({user_id:user.id,socket_id:user.payload[0].meta.socket_id});
    });
    console.log(sockets);
  });


  $("#lsit-delete-group").click(function(event){
    var data = $("#sendMessage").data("info_group");
    console.log($("#sendMessage").data('info_group'));
    alertify.confirm("Eliminar grupo","¿Estás seguro de que desea eliminar el grupo?",function(){
      client.emit("delete",JSON.stringify($("#sendMessage").data('info_group')))
    },function(){

    });
  });
  $("#lsit-leave-group").click(function(){
    var data = $("#sendMessage").data("info_group");
    console.log($("#sendMessage").data('info_group'));
    alertify.confirm("Abandonar grupo","¿Estás seguro de que deseas abandonar el grupo?",function(){
      client.emit("leave",JSON.stringify($("#sendMessage").data('info_group')))
      client.leaveRoom(data.nombre, {}, function (error, left) {
        // status
        console.log(error,left);
      })
    },function(){

    });
  })
  client.on("leaveall",function(room,data){
    alertify.success(data.message);
  });
  client.on("leave",function(data){
    try {
      console.log(data);
      alertify.success(data.message);
      if(data.status==200){
        $.each($("#chatList .group-list"),function(index,object){
          var datos = $(object).data("info");
          console.log(datos);
          if(datos.nombre == data.data.nombre){
            $(object).remove();
            if($(object).hasClass('active')){
                $("#message_list").empty();
                $("#sendMessage .btn").addClass("disabled");
                $("#title-group").text('');
            }
          }
        });
      }
    } catch (e) {
      console.log(e)
    }
  });
  client.on("delete",function(room,data){
    try {
      console.log(data);
      alertify.success(data.message);
      if(data.status==200){
        $.each($("#chatList .group-list"),function(index,object){
          var datos = $(object).data("info");
          console.log(datos);
          if(datos.nombre == data.data.nombre){
            $(object).remove();
            if($(object).hasClass('active')){
                $("#message_list").empty();
                $("#sendMessage .btn").addClass("disabled");
                $("#title-group").text('');
            }
          }
        });
      }
    } catch (e) {
      console.log(e);
    }
  })
  /*client.leaveRoom('lobby', {}, function (error, left) {
    // status
  })*/
  $("#btn-newGroup").click(function(){
    var socket_user=[];
    $.each(sockets,function(i,socket){
       $.each(finded.items,function(index,user_id){
           if(socket.user_id == user_id){
               socket_user.push(socket.socket_id);
           }
       });
    });
    var grupo = {
      nombre   : $('#nombre').val(),
      miembros :finded.items,
      sockets  : socket_user
    };
    client.emit('message',JSON.stringify(grupo));
  });

  $('#chatList').on('click','.clearfix',function(e){
        $("#sendMessage").data('info_group',$(this).data('info'));
        //code to get messages
        let data = $("#sendMessage").data('info_group');
        if(data !== undefined){
            $('#message_list').empty();
            client_chat.emit("greet",JSON.stringify(data));
            $(".clearfix.group-list").removeClass("active");
            $(this).addClass("active");
            $("#sendMessage .btn").removeClass("disabled");
            $("#title-group").text("Grupo: "+data.nombre);
        }

  });
  client_chat.on("greet",function(msgs){
        let dts_msgs = msgs;
        console.log(dts_msgs);
        $.each(dts_msgs,function(i,msg){
            if(msg.id === $("#principal").data('id')){
                $('#message_list').append(groupComponents.list.myMessage(msg));
            }else{
                $('#message_list').append(groupComponents.list.message(msg));
            }
        });
        $('.chat_area').animate({scrollTop:$('.chat_area').prop('scrollHeight')},500);

  });
});
