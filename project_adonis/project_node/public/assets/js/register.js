$(function(){
  $("#btn-register").click(function(event){
    var _csrf = $("input[name='_csrf']").val();
    var data = {
      username: $("input[name='username']").val(),
      email: $("input[name='email']").val(),
      password: $("input[name='password']").val(),
      _csrf : _csrf
    };
    $.ajax({
      "headers": { 'csrftoken' : _csrf },
      "url":"/registrar",
      "type":'POST',
      "data":data
    }).done(function(response){
        if(response.status === 200){
            alert(response.message);
            document.location ="/";
            console.log(response);
            data = response;
        }
    }).fail(function(error){
      alert(error.responseJSON.error.message);
      console.log(error.responseJSON.error.frames);
    });
  });
});
