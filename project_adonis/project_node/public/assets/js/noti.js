var noti = {
    default:function(dst){
        // Let's check if the browser supports notifications
          if (!("Notification" in window)) {
            alert("This browser does not support desktop notification, please ... you change browser");
          }

          // Let's check whether notification permissions have already been granted
          else if (Notification.permission === "granted") {
            // If it's okay let's create a notification
            dst.titulo = (dst.titulo == undefined) ? "Notification" : dst.titulo;
            var notification = new Notification(dst.titulo,
                                                {body:dst.message,
                                                 icon:(dst.image == undefined) ? "/packages/images/Curiosity.png" : dst.image
                                                });
          }

          // Otherwise, we need to ask the user for permission
          else if (Notification.permission !== 'denied') {
            Notification.requestPermission(function (permission) {
              // If the user accepts, let's create a notification
              if (permission === "granted") {
                var notification = new Notification("Hi there!");
              }
            });
          }
    },
    sound:function(){
        let audio = new Audio();
        audio.src = 'assets/npm-debug.mp3';
        audio.play();
    }
}
