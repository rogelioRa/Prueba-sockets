var groupComponents = {
    list:{
        group:(dts)=>{
            let cpnts = '<li class="left clearfix group-list" data-info=\''+JSON.stringify(dts)+'\'>'+
                            '<span class="chat-img pull-left">'+
                                 '<img src="/assets/images/group.png" alt="User Avatar" class="img-circle">'+
                            '</span>'+
                            '<div class="chat-body clearfix">'+
                                '<div class="header_sec">'+
                                   '<strong class="primary-font">Grupo:</strong> <strong class="pull-right">'+
                                   '</strong>'+
                                '</div>'+
                                '<div class="contact_sec">'+
                                   '<strong class="primary-font">'+dts.nombre+'</strong></span>'+
                                '</div>'+
                             '</div>'+
                          '</li>';
            return cpnts;
        },
        message:(dts)=>{
            let cpnts = '<li class="left clearfix">'+
                         '<span class="chat-img1 pull-left" title='+dts.username+'>'+
                         '<div style="border-radius:100%; background:#4258a0; color:#fff; padding:.9rem;">'+dts.username.toLocaleUpperCase().substring(0, 1)+'</div>'+
                         '</span>'+
                         '<div class="chat-body1 clearfix" id="talkbubble">'+
                            '<p>'+dts.content+'</p>'
                         '</div>'+
                      '</li>';
            return cpnts;
        },
        myMessage:(dts)=>{
            let cpnts = '<li class="left clearfix">'+
                         '<span class="chat-img1 pull-right" title='+dts.username+'>'+
                         '<div style="border-radius:100%; background:#4258a0; color:#fff; padding:.9rem;">'+dts.username.toLocaleUpperCase().substring(0, 1)+'</div>'+
                         '</span>'+
                         '<div class="chat-body2 clearfix chat-left" id="talkbubble2">'+
                            '<p>'+dts.content+'</p>'
                         '</div>'+
                      '</li>';
            return cpnts;
        }
    },
    participant:{
        down:(text)=>{
            let cpnts = '<a class="dropdown-item" href="#">'+text+'</a>';
            return cpnts;
        }
    },
    actions:{
        delete:(grupo_id)=>{
            let cpnts = '<li><a href="#" data-action="delete" data-id="'+grupo_id+'">Eliminar grupo</a></li>';
            return cpnts;
        }
    }
}
