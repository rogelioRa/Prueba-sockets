$(function(){
  // or available as global when using the script file from CDN
  const io = ws('http://localhost:3333');
  const client = io.channel('post').connect(console.log);
  const ul = document.getElementById("list");
  client.on("message",function(message){
    console.log(message);
    objectmanagement.addOneComment(JSON.parse(message));
    $("#comment").val('');
  });
  client.on("presence:state",function(state){
    console.log(state);
    const users = state.map(function(user){
      return '<li>'+user.payload[0].meta.email +' is connected from '+ user.payload.length +' divices</li>';
    });
    ul.innerHTML = users.join('');
  });
  $("#btn-send-comment").click(function(event){
    var comment = $("#comment").val();
    if ( comment != '' ){
        client.emit('message',comment);
        //objectmanagement.sendComment();
    }
  });
  var objectmanagement ={
    commetns: [],
    _csrf : $("input[name='_csrf']").val(),
    getComments: function(){
      $.ajax({
        "headers": { 'csrftoken' : objectmanagement._csrf },
        url: "getComments",
        type: "GET"
      }).done(function(response){
        //console.clear();
        console.log(response);
        objectmanagement.comments = response;
        objectmanagement.fillContentWithComments(objectmanagement.comments);
      }).fail(function(error){
        console.error(error);
      });
    },
    sendComment: function(comment){
      $.ajax({
        url:"/sendComment",
        type:"get",
        data:{comment:comment}
      }).done(function(response){
        console.log(response);
        $("#comment").val('');
        objectmanagement.addOneComment(response);
      }).fail(function(error){
        console.error(error);
      });
    },
    addOneComment: function(comment){
      var $comment = $('<div class="row">'+
          '<div class="col-sm-2 col-12">'+
              '<img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-5.jpg">'+
          '</div>'+
          '<div class="col-sm-10 col-12">'+
              '<a><h3 class="user-name">'+comment.email+'</h3></a>'+
              '<div class="card-data">'+
                  '<ul>'+
                      '<li class="comment-date"><i class="fa fa-clock-o"></i> '+comment.fecha+'</li>'+
                  '</ul>'+
              '</div>'+
              '<p class="comment-text">'+comment.text+
              '</p>'+
          '</div>'
        );
        $("#comments-list-post").append($comment);
    },
    fillContentWithComments : function(comments){
      $.each(comments,function(index,object){
        var $comment = $('<div class="row">'+
            '<div class="col-sm-2 col-12">'+
                '<img src="https://mdbootstrap.com/img/Photos/Avatars/avatar-5.jpg">'+
            '</div>'+
            '<div class="col-sm-10 col-12">'+
                '<a><h3 class="user-name">'+object.email+'</h3></a>'+
                '<div class="card-data">'+
                    '<ul>'+
                        '<li class="comment-date"><i class="fa fa-clock-o"></i> '+object.fecha+'</li>'+
                    '</ul>'+
                '</div>'+
                '<p class="comment-text">'+object.text+
                '</p>'+
            '</div>'
          );
          $("#comments-list-post").append($comment);
      });
    }
  };
  objectmanagement.getComments();
});
