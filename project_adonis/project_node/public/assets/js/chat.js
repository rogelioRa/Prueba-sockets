$(function(){
  // or available as global when using the script file from CDN
  const io = ws('http://localhost:3333');
  const client = io.channel('chat').connect(console.log);

  client.on("message",function(message){
    console.log(message);
  });

  client.on("presence:state",function(state){
    console.log(state);
  });

  $("#btn-send-message").click(function(event){
    var comment = $("#message").val();
    if ( comment != '' ){
        client.emit('message',comment);
        //objectmanagement.sendComment();
    }
  });

  $("#btn-save-group").click(function(event){
    var room = {};
    client.emit('message',room);
    room.push(room);
    var groupo = {
      nombre : "grupo",
      miembros:[]
    };
  });
});
