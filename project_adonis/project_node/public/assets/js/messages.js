$(function(){
  var escrito = 0;
  client_chat.on('message', function (room, message) {
    try{
        $('#message_input').val();
        let dts_msgs = message
        let data = $("#sendMessage").data('info_group');


        $('#message_input').val('');
        if(data != undefined){
            if(dts_msgs.group_id === data.id){
                if(dts_msgs.user_id === $("#principal").data('id')){
                    $('#message_list').append(groupComponents.list.myMessage(dts_msgs));
                }else{
                    alertify.message(dts_msgs.username+ ' te ha enviado un mensaje al grupo '+room);
                    noti.default({titulo:dts_msgs.username,message:dts_msgs.content});
                    $('#message_list').append(groupComponents.list.message(dts_msgs));
                }
            }else{
                alertify.message(dts_msgs.username+ ' te ha enviado un mensaje al grupo '+room);
                noti.default({titulo:dts_msgs.username,message:dts_msgs.content});
            }
            $('.chat_area').animate({scrollTop:$('.chat_area').prop('scrollHeight')},500);
        }else{
            alertify.message(dts_msgs.username+ ' te ha enviado un mensaje al grupo '+room);
            noti.default({titulo:dts_msgs.username,message:dts_msgs.content});
        }
        noti.sound();
    }catch(e){
       console.log(e);
    }

  });

  client_chat.on('typing', function (room, message) {
    try{
        let dts_msgs = message
        let data = $("#sendMessage").data('info_group');


        console.log(dts_msgs);
        if(data !== undefined){
            if(dts_msgs.group_id === data.id){
                var st;
                if(dts_msgs.user_id !== $("#principal").data('id')){
                    if(escrito < 1){
                        $("#isTyping").html(dts_msgs.username + ' esta escribiendo...');
                        $("#isTyping").show('slow');
                        escrito = 1;
                        st = setTimeout(function(){
                            $("#isTyping").hide('slow');
                            escrito = 0;
                        }, 2100);
                    }else{
                        clearTimeout(st);
                        st = setTimeout(function(){
                            $("#isTyping").hide('slow');
                            escrito = 0;
                        }, 2100);
                    }
                }

            }
        }
    }catch(e){
       console.log(e);
    }

  });


  $("#sendMessage").click(function(){
    let data = $(this).data('info_group');
    if(data != undefined){
        if($('#message_input').val() !== ''){
            var message = {
              content : $('#message_input').val(),
              id_group:data.id,
              room:data.nombre
            };
            client_chat.emit('message',JSON.stringify(message));
        }
        else{
            alertify.warning('No puedes enviar un mensaje vacio');
        }
    }else{
        console.log(data);
        alertify.message("Elige primero un grupo");
    }
  });
  $('#message_input').keydown(function(){
    let data = $("#sendMessage").data('info_group');
    if(data !== undefined){
       client_chat.emit('typing',{room:data.nombre,group_id:data.id});
    }
  });

  $("#download_messages").click(function(){
      let data = $("#sendMessage").data('info_group');
      if(data !== undefined){
        $.ajax({
            url:'/download-messages',
            method:'POST',
            dataType:'JSON',
            data:{room:data.nombre}
        }).done(function(response){
            alertify.message("Espera mientras que el archivo se descarga");
        }).fail(function(error){
            console.log(error);
        });
      }else{
        alertify.warning("No hay ninguna conversación seleccionada");
      }
  });
});
